﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Camera resizer.
/// This class attach to the camera for resizing its orthographic size to fit with be background, hence it won't affect to other compoent position.
/// This class should be triggered when the scene starts.
/// </summary>
public class CameraResizer : MonoBehaviour {
	public SpriteRenderer baseBackground;

	// Use this for initialization
	void Start () {
		GetComponent<Camera> ().orthographicSize = baseBackground.bounds.size.y/2;
//		Debug.Log ((float)Screen.width + " " + (float)Screen.height);
//		Debug.Log (baseBackground.bounds.size.x/baseBackground.bounds.size.y);

		if ((float)Screen.width/(float)Screen.height < baseBackground.bounds.size.x/baseBackground.bounds.size.y)
		{
//			Debug.Log("width based");
			GetComponent<Camera> ().orthographicSize = baseBackground.bounds.size.y/2 * (1f + (baseBackground.bounds.size.x/baseBackground.bounds.size.y - (float)Screen.width/(float)Screen.height));
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
