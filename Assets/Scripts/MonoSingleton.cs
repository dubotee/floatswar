﻿using UnityEngine;
using System.Collections;

public abstract class MonoSingleton<T> : MonoBehaviour where T:MonoSingleton<T>
{
	private static T g_instance = null;

	public static T Instance
	{
		get { return g_instance; }
	}

	void Awake()
	{
		g_instance = this as T;
		g_instance.OnAwake();
	}
	
	protected virtual void OnAwake()
	{		
	}

    protected virtual void OnDestroy()
    {
        g_instance = null;
    }

	// Make sure the instance isn't referenced anymore when the user quit, just in case.
	private void OnApplicationQuit()
	{
		#if !UNITY_EDITOR
		g_instance = null;
		#endif
	}
}
