﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoSingleton<GameManager>
{
	public enum GameState
	{
		init = 0,
		start,
		play,
		disconnect,
		finish
	}
	
	public enum GameResult
	{
		none = 0,
		win,
		lose,
	}

	[HideInInspector]
	public List<GameObject> gears = new List<GameObject> ();
	
	[HideInInspector]
	public GameState State;
	
	[HideInInspector]
	public GameResult Result;

	
	[Header ("Float")]
	public GameObject redFloatPrefab;
	public GameObject blueFloatPrefab;
	public int initHP = 5;
	public float spinSpeed = 50.0f;
	public int gearNeedToGetThorn = 3;
	public float pushForce = 4000f;
	
	[Header ("Gear")]
	public GameObject gearPrefab;
	public float gearSpawnIntervalRangeMin = 6.0f;
	public float gearSpawnIntervalRangeMax = 12.0f;
	public int gearMax = 9;
	public int gearSpawnInitNum = 4;
	[HideInInspector]
	public float gearSpawnClearRadius = 0.4f;

	// Use this for initialization
	public void StartSpawnGear () {
		//The server spawn and handle all the gear, the client just displays and gets gear results from server
		if (Network.isServer)
		{
			StartCoroutine(SpawnGear(0.0f, gearSpawnInitNum));
		}
	}
	
	IEnumerator SpawnGear(float waitTime, int initSpawn = 1) {
		yield return new WaitForSeconds(waitTime);
		if (gearPrefab != null)
		{
			for (int i = 0; i < initSpawn; i++)
			{
				if (gears.Count >= gearMax) break;
				int tries = 0;
				Vector2 newPos = new Vector2(Random.Range(-4f, 4f), Random.Range(-2.5f, 2.5f));
				while (Physics2D.OverlapCircle(newPos, gearSpawnClearRadius))
				{
//					Debug.Log("init pos again");
					newPos = new Vector2(Random.Range(-4f, 4f), Random.Range(-2.5f, 2.5f));
					tries++;
					if (tries >= 20) break;
				}
				if (tries < 20)
				{
					GameObject newGear = (GameObject)Network.Instantiate(gearPrefab, newPos, Quaternion.identity, 0);
					lock(gears)
					{
						gears.Add(newGear);
					}
				}
				//If the tried 20 times but didn't find suitable position (don't overlapp with other objects), just skip this round

//				else
//				{
//					Debug.Log("Spawn Gear Failed: " + newPos);
//				}
			}
		}

		if (State == GameState.play)
			//as long as the game state is "Play", spawning the gear over and over
			StartCoroutine(SpawnGear(Random.Range(gearSpawnIntervalRangeMin, gearSpawnIntervalRangeMax)));
	}
}
