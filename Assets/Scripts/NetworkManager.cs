using UnityEngine;
using System.Collections;

public class NetworkManager : MonoSingleton<NetworkManager>
{
	//type name and room name, the room name is fixed for demo purpose. Need to improve for real one.
	private const string typeName = "gloops.com.FloatsWar";
	private const string gameName = "room1";
	
	private bool isRefreshingHostList = false;
	// contain all host
	private HostData[] hostList;
	
	void OnGUI()
	{
		//Create simple GUI buttons
		if (!Network.isClient && !Network.isServer)
		{
			if (GUI.Button(new Rect(30, 30, 100, 60), "Start Server"))
				StartServer();
			
			if (GUI.Button(new Rect(30, 120, 100, 60), "Refresh Hosts"))
				RefreshHostList();
			
			if (hostList != null)
			{
				for (int i = 0; i < hostList.Length; i++)
				{
					if (GUI.Button(new Rect(200, 30 + (110 * i), 100, 60), hostList[i].gameName))
					{
						JoinServer(hostList[i]);
					}
				}
			}
		}

		//Show the message when server is waiting for its client.
		if (Network.isServer && GameManager.Instance.State == GameManager.GameState.init) {
			GUI.Label(new Rect(30, 30, 200, 60), "Waiting for opponent...");
		}

		//Show the game result once the game state turn to "finish".
		//Show the restart button, this one will be 
		if (GameManager.Instance.State == GameManager.GameState.finish)
		{
			GUI.Label(new Rect(30, 30, 100, 60), GameManager.Instance.Result != GameManager.GameResult.none ? GameManager.Instance.Result == GameManager.GameResult.win ? "You Win" : "You Lose" : "...");
			
			if (GUI.Button(new Rect(30, 120, 100, 60), "Restart"))
			{
				Network.Disconnect();
				Application.LoadLevel("Dummy");
			}
				
		}
	}

	//Start server
	private void StartServer()
	{
		Network.InitializeServer(2, 25000, !Network.HavePublicAddress());//
		MasterServer.RegisterHost(typeName, gameName);
	}

	//Overriden event function. 
	//Load the dummy scene (to restart the whole game)
	void OnDisconnectedFromServer(NetworkDisconnection info) {
		Application.LoadLevel("Dummy");
	}

	//Overriden event function. 
	//Load the dummy scene (to restart the whole game). Try to close the host if this one is server
	void OnPlayerDisconnected(NetworkPlayer player) {
		if (Network.isServer) {
			Network.Disconnect();
			MasterServer.UnregisterHost();
		}
		Application.LoadLevel("Dummy");
	}
	
	//Overriden event function. 
	//Create the main game object then the server is started.
	void OnServerInitialized()
	{
		SpawnAvatar();
	}

	void Update()
	{
		//Try to refresh the host list on demand.
		if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
		{
			isRefreshingHostList = false;
			hostList = MasterServer.PollHostList();
		}
	}

	//Refresh the host list.
	private void RefreshHostList()
	{
		if (!isRefreshingHostList)
		{
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}

	//The client try to joins the game room
	private void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
	}
	
	//Overriden event function. 
	//Start the game on client site once this client connected to the server.
	void OnConnectedToServer()
	{
		SpawnAvatar();
		GameManager.Instance.State = GameManager.GameState.play;
	}

	//Create main game object (the floats)
	private void SpawnAvatar()
	{
		if (Network.isServer) 
		{
			Network.Instantiate (GameManager.Instance.redFloatPrefab, new Vector3 (-2.75f, 2f, 0), Quaternion.identity, 0);
		}
		else
		{
			Network.Instantiate (GameManager.Instance.blueFloatPrefab, new Vector3 (2.75f, -2f, 0), Quaternion.identity, 0);
		}
	}
	
	//Overriden event function. 
	//Start the game on server site once any client connected to this server.
	void OnPlayerConnected(NetworkPlayer player)
	{
		if (Network.isServer)
		{
			GameManager.Instance.State = GameManager.GameState.play;
			GameManager.Instance.StartSpawnGear();
		}
	}
}
