﻿using UnityEngine;
using System.Collections;

public class FloatController : MonoBehaviour {
	// variables for syn data
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;
	private Vector3 syncRotation = Vector3.zero;

	public Sprite normalSprite;
	public Sprite thornSprite;

	bool isSpinning = true;
	bool isAlive = true;

	int HP = 5;
	int gearCummulative = 0;
	bool hasThorn = false;
	
	float dirRotateSpeed;

	// Use this for initialization
	void Start () {
		//Randomly init the rotation
		transform.rotation = Quaternion.Euler (new Vector3(0, 0, Random.Range(0, 360)));

		isSpinning = true;
		isAlive = true;

		dirRotateSpeed = GameManager.Instance.spinSpeed;
		HP = GameManager.Instance.initHP;
	}
	
	//Overriden event function. 
	//Default snippet for this network view to observe the game object change.
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		//We try to sync position and velocity of this object with its other instance, from other machine
		//Addtionally, we need HP for displaying the HP bar of this game object on other machine, and thorn flag for other machine will know that whether our Float is capable to kill its, lastly, the rotation diretion for others also need to know our current rotation direction.
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		bool syncThorn = false;
		int syncHP = HP;

		//if this game object is our property, try to update its informations to its other instance, from other machine
		if (stream.isWriting)
		{
			syncPosition = new Vector3(GetComponent<Rigidbody2D>().position.x, GetComponent<Rigidbody2D>().position.y, 0);
			stream.Serialize(ref syncPosition);
			
			syncVelocity =  new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0);
			stream.Serialize(ref syncVelocity);

			//those three other essential variables
			syncRotation = transform.rotation.eulerAngles;
			stream.Serialize(ref syncRotation);

			syncThorn = hasThorn;
			stream.Serialize(ref syncThorn);

			syncHP = HP;
			stream.Serialize(ref syncHP);
		}
		//or else, try to get it informations from its other instance, from other machine
		else
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			stream.Serialize(ref syncRotation);
			stream.Serialize(ref syncThorn);
			stream.Serialize(ref syncHP);
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;

			//the trick is not writing the velocity directly to its rigidbody2D
			//just interpolate the next position and update that one.
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody2D>().position;

			//those three other essential variables
			transform.rotation = Quaternion.Euler( syncRotation );
			hasThorn = syncThorn;
			HP = syncHP;
		}
	}

	void Awake()
	{
		//Default snippet for this network view to observe the game object change.
		lastSynchronizationTime = Time.time;
		GetComponent<NetworkView> ().observed = this;
	}
	
	// Update is called once per frame
	void Update () {
		//Update the game sprite based on whether this game object has throne
		GetComponentInChildren<SpriteRenderer> ().sprite = hasThorn ? thornSprite : normalSprite;

		//Just update the game object if it is property of this machine
		if(GetComponent<NetworkView>().isMine)
		{
			if (isAlive && GameManager.Instance.State == GameManager.GameState.play) 
			{
				//rotate the float frame by frame is this one is not moving
				if (isSpinning)
					transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles + new Vector3 (0, 0, dirRotateSpeed * Time.deltaTime));

				//try to move if this one is not moving and the user tap on it
				if (isSpinning && Input.GetMouseButton (0)) {
					Vector3 rotatedVector = transform.rotation * new Vector3 (0, 1, 0);
					GetComponent<Rigidbody2D> ().AddForce (rotatedVector.normalized * GameManager.Instance.pushForce);
					isSpinning = false;
				}

				//stop and spinning this one if when it slow down to certain speed
				if (!isSpinning && GetComponent<Rigidbody2D> ().velocity.magnitude < 1.0f) {
					ResetSpinning ();
				}
			}

			//Try to equip throne when this one get enough gear
			if (gearCummulative > GameManager.Instance.gearNeedToGetThorn)
			{
				hasThorn = true;
			}

			//yes, die
			if (HP <= 0 && isAlive) {
				ResetSpinning ();
				isAlive = false;
				GameManager.Instance.State = GameManager.GameState.finish;
				GameManager.Instance.Result = GameManager.GameResult.lose;
				//Send the other that he has won this game.
				GetComponent<NetworkView>().RPC("GetDead", RPCMode.OthersBuffered);
			}
		}
		//if not mine, just update the position based one interpolated position from OnSerializeNetworkView()
		else
		{
			SyncedMovement();
		}
	}

	//this one is Remote Produce Call
	[RPC]
	public void GetDead()
	{
		ResetSpinning ();
		isAlive = false;
		GameManager.Instance.State = GameManager.GameState.finish;
		GameManager.Instance.Result = GameManager.GameResult.win;
		Network.Destroy (this.gameObject);
	}

	//Stop the float and try to spin it again
	void ResetSpinning()
	{
		isSpinning = true;
		dirRotateSpeed = GetComponent<Rigidbody2D> ().angularVelocity * dirRotateSpeed > 0 ? dirRotateSpeed : -dirRotateSpeed;
		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		GetComponent<Rigidbody2D> ().angularVelocity = 0.0f;
	}

	//Overriden event function. When it collide with a trigger (in this case is Gear)
	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.GetComponent<Gear>() != null && !isSpinning) {
			ResetSpinning();
			GameManager.Instance.gears.Remove(other.gameObject);
			Network.Destroy(other.gameObject);
			gearCummulative++;
		}
	}

	//Overriden event function. When it collide with another collider (Thorn border or another Float)
	void OnCollisionEnter2D(Collision2D col)
	{
		//If the collider is a Thorn border
		if (col.gameObject.GetComponent<Thorn> () != null) 
		{
			HP -= 1;
		}
		//If another float
		else
		{
			//if this one is not our property, its not responsibilty to handle this
			if (!GetComponent<NetworkView> ().isMine) 
			{
				//Try to send for owner to handler this event
				GetComponent<NetworkView>().RPC("AddCollistionForce", RPCMode.OthersBuffered, GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
			}
			//if this one is mine
			else
			{
				if (col.gameObject.GetComponent<FloatController>() != null)
				{
					//try to reduce HP, is the opposive float has thorn, our HP is reduce to 0 immediately
					float dotval = Vector2.Dot(col.gameObject.transform.rotation * new Vector3 (0, 1, 0), transform.position - col.gameObject.transform.position);
					if (col.gameObject.GetComponent<FloatController>().hasThorn && dotval > 0.6f)
					{
						HP = 0;
					}
				}
			}
		}
	}

	//this one is Remote Produce Call
	[RPC]
	public void AddCollistionForce(float velocityX, float velocityY)
	{
		//Just need to get velocity from the other machine and add to our own float
		isSpinning = false;
		GetComponent<Rigidbody2D>().velocity = new Vector2(velocityX, velocityY);
	}

	//update the position based one interpolated position from OnSerializeNetworkView()
	private void SyncedMovement()
	{
		syncTime += Time.deltaTime;
		GetComponent<Rigidbody2D>().position = Vector2.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}

	//Used for drawing HP bar on OnGUI()
	void DrawQuad(Rect position, Color color) {
		Texture2D texture = new Texture2D(1, 1);
		texture.SetPixel(0,0,color);
		texture.Apply();
		GUI.skin.box.normal.background = texture;
		GUI.Box(position, GUIContent.none);
	}

	//Mainly just draw HP bar
	void OnGUI ()
	{
		if (isAlive && GameManager.Instance.State == GameManager.GameState.play) 
		{
			Vector2 translatedVec = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x, - transform.position.y));

			DrawQuad(new Rect(translatedVec.x - Screen.width/20f, translatedVec.y - Screen.height/10f, Screen.width/10f, Screen.height/50f), Color.white);
			DrawQuad(new Rect(translatedVec.x - Screen.width/20f, translatedVec.y - Screen.height/10f, Screen.width/10f * HP/ GameManager.Instance.initHP, Screen.height/50f), Color.red);
		}
	}
}
