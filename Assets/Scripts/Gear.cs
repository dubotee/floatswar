﻿using UnityEngine;
using System.Collections;

public class Gear : MonoBehaviour {
	// variables for syn data
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;

	//Overriden event function. 
	//Default snippet for this network view to observe the game object change.
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		//We try to sync position and velocity of this object with its other instance, from other machine
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;

		//if this game object is our property, try to update its informations to its other instance, from other machine
		if (stream.isWriting)
		{
			syncPosition = new Vector3(GetComponent<Rigidbody2D>().position.x, GetComponent<Rigidbody2D>().position.y, 0);
			stream.Serialize(ref syncPosition);
			
			syncVelocity =  new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0);
			stream.Serialize(ref syncVelocity);
		}
		//or else, try to get it informations from its other instance, from other machine
		else
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;

			//the trick is not writing the velocity directly to its rigidbody2D
			//just interpolate the next position and update that one.
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
			syncStartPosition = GetComponent<Rigidbody2D>().position;
		}
	}
	
	void Awake()
	{
		//Default snippet for this network view to observe the game object change.
		lastSynchronizationTime = Time.time;
		GetComponent<NetworkView> ().observed = this;
	}

	public float rotateSpeed = 30.0f;

	// Use this for initialization
	void Start () {
		//Randomly init the rotation
		transform.rotation = Quaternion.Euler (new Vector3(0, 0, Random.Range(0, 360)));
	}
	
	// Update is called once per frame
	void Update () {
		//rotate the gear frame by frame
		transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles + new Vector3 (0, 0, rotateSpeed * Time.deltaTime));

		//if not mine, just update the position based one interpolated position from OnSerializeNetworkView()
		if (!GetComponent<NetworkView>().isMine)
		{
			SyncedMovement();
		}
	}

	//update the position based one interpolated position from OnSerializeNetworkView()
	private void SyncedMovement()
	{
		syncTime += Time.deltaTime;
		GetComponent<Rigidbody2D>().position = Vector2.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}
}
